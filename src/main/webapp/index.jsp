<%-- 
    Document   : index
    Created on : 01-07-2021, 23:12:57
    Author     : luisg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
        <title>JSP Page</title>
    </head>
    <style>
        *{
            
            padding: 0;
        }    
        body{
            background: #069;
        }
        h1{
            color:white;
            font-size: 40px;
            text-align: center;
            font-family: 'Roboto Condensed', sans-serif;
        }
         h2{
            color:white;
            font-size: 35px;
            text-align: center;
            font-family: 'Roboto Condensed', sans-serif;
        }
    </style>
    <body>
        <h1><strong>Endpoints Api Comunas</strong></h1><br>
        <h2>Lista de comunas - GET - https://solemne3luisgarcia.herokuapp.com/api/comunas</h2>
        <br>
        <h2>Lista de comunas por id - GET - https://solemne3luisgarcia.herokuapp.com/api/{id}</h2>
        <br>
        <h2>Crear comuna - POST - https://solemne3luisgarcia.herokuapp.com/api/comunas</h2>
        <br>
        <h2>Actualizar comuna - PUT - https://solemne3luisgarcia.herokuapp.com/api/comunas</h2>
        <br>
        <h2>Eliminar comuna - DELETE - https://solemne3luisgarcia.herokuapp.com/api/comunas</h2>
    </body>
</html>
